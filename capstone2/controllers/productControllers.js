const Product = require("../models/product");
const mongoose = require("mongoose");

// C R E A T E   P R O D U C T (A D M I N)
module.exports.createProduct = (data) => {
    console.log(data)
    if(data.isAdmin == true) {
        let newProduct = new Product({
            productName: data.product.productName,
            description: data.product.description,
            price: data.product.price
        });
        return newProduct.save().then((product, error) => {
            if(error){
                return false
            }
            return newProduct
        })
    }
    let message = Promise.resolve('User must be ADMIN to access this.')

    return message.then((value) => {
        return {value}
    })
};