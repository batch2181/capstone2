const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");

// R E G I S T R A T I O N
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => 
        res.send(resultFromController))
});

// L O G I N
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => 
        res.send(resultFromController))
})














module.exports = router;