const express = require("express")
const router = express.Router();
const productController = require("../controllers/productControllers.js");
const auth = require("../auth.js");

// C R E A T E   P R O D U C T (A D M I N)
router.post("/create", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data.isAdmin)
    productController.createProduct(data).then(resultFromController => 
        res.send(resultFromController));
});





module.exports = router;